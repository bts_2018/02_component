# What is Angular? #
AngularJS is a structural framework for dynamic web apps power by google.
It lets you use HTML as your template language and lets you extend HTML's syntax to express your application's 
components clearly and succinctly. 
Angular's data binding and dependency injection eliminate much of the code you currently have to write. 

- Angular provides developers options to write client side application (using JavaScript) in a clean MVC(Model View Controller) way.

- Application written in Angular is cross-browser compliant. Angular automatically handles JavaScript code suitable for each browser.

- Angular is open source, completely free, and used by thousands of developers around the world.


# What is Angular CLI? #
Angular CLI stands for Angular Command Line Interface. As the name implies, it is a command line tool for creating angular apps. It is recommended to use angular cli for creating angular apps as you don't need to spend time installing and configuring all the required dependencies and wiring everything together.


# install angular  #
npm install -g @angular/cli

 
# new project#
ng new MyApp

# new component#
ng g c namecomp

it generates the component and adds the component to module declarations.

	:::javascript
	create src/app/test/namecomp.component.css (0 bytes)
	create src/app/test/namecomp.component.html (23 bytes)
	create src/app/test/namecomp.component.spec.ts (614 bytes)
	create src/app/test/namecomp.component.ts (261 bytes)
	update src/app/app.module.ts (1087 bytes)


# execute #
ng serve 
